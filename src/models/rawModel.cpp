#include "models/rawModel.h"

RawModel::RawModel(unsigned int VAOId, unsigned int vertexCount)
	: _VAOId(VAOId), _vertexCount(vertexCount)
{

}

unsigned int RawModel::getVAOId() const {
	return _VAOId;
}

unsigned int RawModel::getVertexCount() const {
	return _vertexCount;
}
