#include "models/texturedModel.h"

TexturedModel::TexturedModel(RawModel model, Texture texture)
	: RawModel(model),
	  _model(model), _texture(texture)
{

}

RawModel TexturedModel::getModel()
{
	return _model;
}

Texture TexturedModel::getTexture()
{
	return _texture;
}
