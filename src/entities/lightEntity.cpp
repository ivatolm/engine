#include "entities/lightEntity.h"

LightEntity::LightEntity(glm::vec3 position, glm::vec3 rotation, glm::vec3 color, glm::vec3 attenuation)
	: RawEntity(RawModel(0, 0), position, rotation, 1),
	  _color(color),
	  _attenuation(attenuation)
{

}

glm::vec3 LightEntity::getColor() {
	return _color;
}

glm::vec3 LightEntity::getAttenuation()
{
	return _attenuation;
}

void LightEntity::setColor(glm::vec3 color) {
	_color = color;
}

void LightEntity::setAttenuation(glm::vec3 attenuation)
{
	_attenuation = attenuation;
}
