#include "entities/terrainEntity.h"
#include "renderEngine/loader.h"
#include "utilities/mathUtility.h"
#include <noise/noise.h>

TerrainEntity::TerrainEntity(glm::vec3 position, unsigned int verticesOnSideNum, float size, float amplitude)
	: RawEntity(RawModel(0, 0), position, {0, 0, 0}, 1), _vertexOnSideNum(verticesOnSideNum), _size(size), _amplitude(amplitude)
{
	generateHeights();
}

void TerrainEntity::loadModel(Loader *loader)
{
	RawModel model = loader->loadTerrainEntity
			(
				generateTerrainMesh(),
				generateIndices(),
				generateNormals()
			);
	_model = model;
}

void TerrainEntity::generateHeights()
{
	noise::module::Perlin PerlinNoise;

	std::vector<std::vector<float>> heights(_vertexOnSideNum, std::vector<float>(_vertexOnSideNum));
	_heights = heights;

	for (unsigned int z = 0; z < _heights.size(); z++)
	{
		for (unsigned int x = 0; x < _heights[0].size(); x++)
		{
			double frequency = 0.001;
			double hegithMulFactor = static_cast<double>(_amplitude);
			_heights[z][x] = static_cast<float>(PerlinNoise.GetValue(x*frequency, 0, z*frequency) * hegithMulFactor);
		}
	}
}

std::vector<float> TerrainEntity::generateTerrainMesh()
{
	float polygonSize = _size / static_cast<float>(_vertexOnSideNum);

	std::vector<float> vertices;
	for (unsigned int z = 0; z < _vertexOnSideNum; z++)
	{
		for (unsigned int x = 0; x < _vertexOnSideNum; x++)
		{
			float vertexX = x * polygonSize;
			float vertexY = _heights[z][x];
			float vertexZ = z * polygonSize;

			vertices.push_back(vertexX);
			vertices.push_back(vertexY);
			vertices.push_back(vertexZ);
		}
	}

	return vertices;
}

std::vector<unsigned int> TerrainEntity::generateIndices()
{
	std::vector<unsigned int> indices;
	for (unsigned int z = 0; z < _vertexOnSideNum-1; z++)
	{
		for (unsigned int x = 0; x < _vertexOnSideNum-1; x++)
		{
			indices.push_back((_vertexOnSideNum*(z+1))+x);
			indices.push_back((_vertexOnSideNum*(z+1))+x+1);
			indices.push_back((_vertexOnSideNum*z)+x);

			indices.push_back((_vertexOnSideNum*z)+x);
			indices.push_back((_vertexOnSideNum*(z+1))+x+1);
			indices.push_back((_vertexOnSideNum*z)+x+1);
		}
	}

	return indices;
}

std::vector<float> TerrainEntity::generateNormals()
{
	/*
	 * This method is generating not completely correct normals for terrain,
	 * because correct normals need terrain to split quiet all of the vertices
	 * six times.
	*/
	std::vector<float> normals;
	for (unsigned int z = 0; z < _vertexOnSideNum; z++)
	{
		for (unsigned int x = 0; x < _vertexOnSideNum; x++)
		{
			normals.push_back(0);
			normals.push_back(1);
			normals.push_back(0);
		}
	}
	return normals;
}

std::pair<bool, float> TerrainEntity::getVertexHeight(float x, float z)
{
	float terrainX = x - _position.x;
	float terrainZ = z - _position.z;
	float polygonSize = _size / (_heights.size() - 1);
	int gridX = static_cast<int>(floor(terrainX / polygonSize));
	int gridZ = static_cast<int>(floor(terrainZ / polygonSize));
	if (gridX >= static_cast<int>(_heights.size()) - 1 || gridZ >= static_cast<int>(_heights.size()) - 1 || gridX < 0 || gridZ < 0)
		return {false, 0.0f};
	float xCoord = fmod(terrainX, polygonSize) / polygonSize;
	float zCoord = fmod(terrainZ, polygonSize) / polygonSize;
	float answer;
	unsigned int UI_gridX = static_cast<unsigned int>(gridX);
	unsigned int UI_gridZ = static_cast<unsigned int>(gridZ);
	if (xCoord <= (1-zCoord)) {
		answer = MathUtility::barryCentric
		(
			glm::vec3(0, _heights[UI_gridZ][UI_gridX], 0),
			glm::vec3(1, _heights[UI_gridZ + 1][UI_gridX + 1], 1),
			glm::vec3(1, _heights[UI_gridZ][UI_gridX + 1], 0),
			glm::vec2(xCoord, zCoord)
		);
	} else {
		answer = MathUtility::barryCentric
		(
			glm::vec3(0, _heights[UI_gridZ][UI_gridX], 0),
			glm::vec3(0, _heights[UI_gridZ + 1][UI_gridX], 1),
			glm::vec3(1, _heights[UI_gridZ + 1][UI_gridX + 1], 1),
			glm::vec2(xCoord, zCoord)
		);
	}
	return {true, answer};
}
