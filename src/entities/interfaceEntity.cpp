#include "entities/interfaceEntity.h"
#include "models/rawModel.h"

InterfaceEntity::InterfaceEntity(RawModel model, glm::vec2 leftTopCorner, glm::vec2 rightBottomCorner, bool isClickable)
	: RawEntity(model, {leftTopCorner.x, leftTopCorner.y, 0}, {0, 0, 0}, 1),
	  _leftTopCorner(leftTopCorner),
	  _rightBottomCorner(rightBottomCorner),
	  _isClickable(isClickable),
	  _clicked(false),
	  _hovered(false)
{

}

glm::vec2 InterfaceEntity::getLeftTopCorner() {
	return _leftTopCorner;
}

glm::vec2 InterfaceEntity::getRightBottomCorner() {
	return _rightBottomCorner;
}

bool InterfaceEntity::getIsClickable() {
	return _isClickable;
}

void InterfaceEntity::setClickedStatus(bool status) {
	_clicked = status;
}

void InterfaceEntity::setHoveredStatus(bool status) {
	_hovered = status;
}

bool InterfaceEntity::isClicked() {
	return _clicked;
}

bool InterfaceEntity::isHovered() {
	return _hovered;
}

