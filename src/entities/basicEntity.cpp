#include "entities/basicEntity.h"

BasicEntity::BasicEntity(TexturedModel model, glm::vec3 position, glm::vec3 rotation, float scale)
	: RawEntity(model, position, rotation, scale),
	  _model(model)
{

}

TexturedModel BasicEntity::getModel()
{
	return _model;
}

glm::vec3 BasicEntity::getVelocity()
{
	return _velocity;
}

glm::vec3 BasicEntity::getAcceleration()
{
	return _acceleration;
}

void BasicEntity::setVelocity(glm::vec3 velocity)
{
	_velocity = velocity;
}

void BasicEntity::setAcceleration(glm::vec3 acceleration)
{
	_acceleration = acceleration;
}
