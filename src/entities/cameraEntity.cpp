#include "entities/cameraEntity.h"
#include "utilities/mathUtility.h"
#include <glm/gtc/type_ptr.hpp>

/*static*/float CameraEntity::_aspectRatio;

CameraEntity::CameraEntity()
	: RawEntity(RawModel(0, 0), glm::vec3(0, 0, 0), {0, 0, 0}, 1)
{
	_fov = 70.0f;
	_nearPlane = 0.1f;
	_farPlane = 1000.0f;
	_lastMousePosition = {-1, -1};
	_sensitivity = 0.1f;
	_follObjOffset = {0.0, 1.0, -1.0};
}

void CameraEntity::update(std::vector<int> mousePosition) {
	if (!(_lastMousePosition[0] == -1 && _lastMousePosition[1] == -1)) {
		float offsetX = mousePosition[0] - _lastMousePosition[0];
		float offsetY = mousePosition[1] - _lastMousePosition[1];

		offsetX *= _sensitivity;
		offsetY *= _sensitivity;

		_rotation.y += offsetX;
		_rotation.x += offsetY;
	}

	if (_rotation.x > 89.0f)
		_rotation.x = 89.0f;
	if (_rotation.x < -89.0f)
		_rotation.x = -89.0f;

	_lastMousePosition = mousePosition;

	_position = _target->getPosition();
	_position += _follObjOffset;
}

/*static */void CameraEntity::setWindowSize(unsigned int width, unsigned int height) {
	_aspectRatio = static_cast<float>(width) / static_cast<float>(height);
}

void CameraEntity::setFollowingTarget(RawEntity *entity)
{
	_target = entity;
}

float CameraEntity::getFOV() {
	return _fov;
}

float CameraEntity::getAspectRatio() {
	return _aspectRatio;
}

float CameraEntity::getNearPlane() {
	return _nearPlane;
}

float CameraEntity::getFarPlane() {
	return _farPlane;
}
