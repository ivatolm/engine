#include "entities/rawEntity.h"

RawEntity::RawEntity(RawModel model, glm::vec3 position, glm::vec3 rotation, float scale)
	: _model(model), _position(position), _rotation(rotation), _scale(scale)
{

}

void RawEntity::increasePosition(glm::vec3 delta) {
	_position += delta;
}

void RawEntity::increaseRotataion(glm::vec3 delta) {
	_rotation += delta;
}

RawModel RawEntity::getModel() {
	return _model;
}

glm::vec3 RawEntity::getPosition() {
	return _position;
}

void RawEntity::setPosition(glm::vec3 position) {
	_position = position;
}

glm::vec3 RawEntity::getRotation() {
	return _rotation;
}

void RawEntity::setRotation(glm::vec3 rotation) {
	_rotation = rotation;
}

float RawEntity::getScale() {
	return _scale;
}

void RawEntity::setScale(float scale) {
	_scale = scale;
}

