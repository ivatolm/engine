#include "engine/engine.h"

Engine::Engine() {
	_displayManager = new DisplayManager;
	_windows.push_back(_displayManager->createWindow(1600, 900, "Engine", false));
	_displayManager->makeContextCurrent(_windows[0]);
	_displayManager->setVSYNC(true);
	_displayManager->setMouseHidden(true);

	_callbackManager = new CallbackManager;
	_callbackManager->setUpCallbacks(_displayManager->getWindowPtr(_windows[0]));
	_callbackManager->addFuncToWRE(CameraEntity::setWindowSize);
	_callbackManager->setProjectionMatrixCreationFunc(MasterRenderer::setProjectionMatrix);

	_camera = new CameraEntity();
	auto windowSize = _callbackManager->getWindowSize();
	_camera->setWindowSize(windowSize[0], windowSize[1]);

	_masterRenderer = new MasterRenderer;
	_masterRenderer->setRenderingWindow(_displayManager->getWindowPtr(_windows[0]));

	_callbackManager->setMasterRendererPtr(_masterRenderer);

	_loader = new Loader;

	_objLoader = new ObjLoader;
	_objLoader->setLoader(_loader);

	_guiManager = new GuiManager;
	_guiManager->setWindow(_displayManager->getWindowPtr(_windows[0]));

	_basicEntityShader = new StaticShader(
							"../engine/res/defaultShaders/vertexShader.glsl",
							"../engine/res/defaultShaders/fragmentShader.glsl"
							);
	_interfaceEntityShader = new StaticShader(
							"../engine/res/defaultShaders/vertexShader.glsl",
							"../engine/res/defaultShaders/fragmentShader.glsl"
							);
	_terrainEntityShader = new StaticShader(
							"../engine/res/defaultShaders/vertexShader.glsl",
							"../engine/res/defaultShaders/fragmentShader.glsl"
							);

	_masterRenderer->setCamera(_camera);
	_masterRenderer->setShaders(_basicEntityShader, _interfaceEntityShader, _terrainEntityShader);
	_masterRenderer->setProjectionMatrix(_masterRenderer);

	_physicsManager = new PhysicsManager;

	_running = true;
}

void Engine::update() {
	if (!_running)
		return;

	glfwPollEvents();

	_physicsManager->update(_displayManager->getDeltaTime());

	_camera->update(_callbackManager->getMousePosition());

	_masterRenderer->render(_basicEntities, _interfaceEntities, _terrainEntities, _lightEntities);

	_guiManager->update(_callbackManager->getMousePosition(), _callbackManager->getMouseInput());

	_running = !_displayManager->isWindowClosed(_windows[0]);
}

bool Engine::isRunning() {
	return _running;
}

void Engine::setEntityShader(const char *vertexShaderFile, const char *fragmentShaderFile)
{
	if (_basicEntityShader)
		delete _basicEntityShader;
	_basicEntityShader = new StaticShader(
							vertexShaderFile,
							fragmentShaderFile
						);
	_masterRenderer->setShaders(_basicEntityShader, _interfaceEntityShader, _terrainEntityShader);
	_masterRenderer->setProjectionMatrix(_masterRenderer);
}

void Engine::setInterfaceShader(const char *vertexShaderFile, const char *fragmentShaderFile)
{
	if (_interfaceEntityShader)
		delete _interfaceEntityShader;
	_interfaceEntityShader = new StaticShader(
								vertexShaderFile,
								fragmentShaderFile
						);
	_masterRenderer->setShaders(_basicEntityShader, _interfaceEntityShader, _terrainEntityShader);
	_masterRenderer->setProjectionMatrix(_masterRenderer);
}

void Engine::setTerrainShader(const char *vertexShaderFile, const char *fragmentShaderFile)
{
	if (_terrainEntityShader)
		delete _terrainEntityShader;
	_terrainEntityShader = new StaticShader(
							vertexShaderFile,
							fragmentShaderFile
						);
	_masterRenderer->setShaders(_basicEntityShader, _interfaceEntityShader, _terrainEntityShader);
	_masterRenderer->setProjectionMatrix(_masterRenderer);
}

void Engine::setCameraTarget(RawEntity *entity)
{
	_camera->setFollowingTarget(entity);
}

void Engine::addBasicEntity(BasicEntity *basicEntity)
{
	_physicsManager->addBasicEntity(basicEntity);
	_basicEntities.push_back(basicEntity);
}

void Engine::addInterfaceEntity(InterfaceEntity *interfaceEntity)
{
	_interfaceEntities.push_back(interfaceEntity);
}

void Engine::addTerrainEntity(TerrainEntity *terrainEntity)
{
	terrainEntity->loadModel(_loader);
	_physicsManager->addTerrainEntity(terrainEntity);
	_terrainEntities.push_back(terrainEntity);
}

void Engine::addLightEntity(LightEntity *lightEntity)
{
	_lightEntities.push_back(lightEntity);
}

RawModel Engine::loadModel(const char *pathToFile)
{
	return _objLoader->loadModel(pathToFile);
}

unsigned int Engine::loadTexture(const char *pathToFile)
{
	return _loader->loadTexture(pathToFile);
}


