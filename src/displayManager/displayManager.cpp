#include "displayManager/displayManager.h"
#include <stdio.h>
#include <stdlib.h>
#include <chrono>

DisplayManager::DisplayManager() {
	if (!glfwInit()) {
		printf("Cannot initialize glfw library.\n");
		exit(-1);
	}
}

DisplayManager::~DisplayManager() {
	glfwTerminate();
}

unsigned int DisplayManager::createWindow(int width, int height, const char *title, bool fullscreen) {
	GLFWwindow *newWindow;
	if (fullscreen)
		newWindow = glfwCreateWindow(width, height, title, glfwGetPrimaryMonitor(), nullptr);
	else
		newWindow = glfwCreateWindow(width, height, title, nullptr, nullptr);
	unsigned int newWindowId = _windowsCounter;
	_windowsPtrs.push_back({newWindowId, newWindow});
	_windowsCounter++;
	return newWindowId;
}

void DisplayManager::deleteWindow(unsigned int windowId) {
	for (unsigned int i = 0; i < _windowsPtrs.size(); i++) {
		if (_windowsPtrs[i].first == windowId) {
			glfwDestroyWindow(_windowsPtrs[i].second);
			_windowsPtrs.erase(_windowsPtrs.begin() + i);
		}
	}
}

void DisplayManager::makeContextCurrent(unsigned int windowId) {
	for (unsigned int i = 0; i < _windowsPtrs.size(); i++)
		if (_windowsPtrs[i].first == windowId) {
			glfwMakeContextCurrent(_windowsPtrs[i].second);
			_currentWindow = _windowsPtrs[i].second;
		}
}

GLFWwindow * DisplayManager::getWindowPtr(unsigned int windowId) {
	for (unsigned int i = 0; i < _windowsPtrs.size(); i++)
		if (_windowsPtrs[i].first == windowId)
			return _windowsPtrs[i].second;
	return nullptr;
}

float DisplayManager::getDeltaTime()
{
	static auto timer = std::chrono::system_clock::now();
	auto currentTime = std::chrono::system_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - timer);
	float deltaTime = static_cast<float>(duration.count()) / 1000.0f;
	timer = currentTime;
	return deltaTime;
}

bool DisplayManager::isWindowClosed(unsigned int windowId) {
	for (unsigned int i = 0; i < _windowsPtrs.size(); i++)
		if (_windowsPtrs[i].first == windowId)
			return glfwWindowShouldClose(_windowsPtrs[i].second);
	return false;
}

void DisplayManager::setVSYNC(bool state) {
	glfwSwapInterval(state);
}

void DisplayManager::setMouseHidden(bool state)
{
	if (state)
		glfwSetInputMode(_currentWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	else
		glfwSetInputMode(_currentWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}
