#include "displayManager/callbackManager.h"
#include "renderEngine/masterRenderer.h"
#include <GLFW/glfw3.h>

/*static */MasterRenderer *CallbackManager::_masterRendererPtr;
/*static */std::vector<void (*)(unsigned int, unsigned int)> CallbackManager::_funcsToCallWRE;
/*static */void (*CallbackManager::_projectionMatrixCreationFunc)(MasterRenderer *);
/*static */std::vector<bool> CallbackManager::_mouseKeys(3);
/*static */std::vector<int> CallbackManager::_mousePosition(2);

void CallbackManager::setUpCallbacks(GLFWwindow *window)
{
	_window = window;
	glfwSetWindowSizeCallback(_window, CallbackManager::window_size_callback);
	glfwSetMouseButtonCallback(_window, CallbackManager::mouseCallback);
}

/*static */void CallbackManager::window_size_callback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
	for (auto function : _funcsToCallWRE)
		(*function)(static_cast<unsigned int>(width), static_cast<unsigned int>(height));
	(*_projectionMatrixCreationFunc)(_masterRendererPtr);
}

/*static */ void CallbackManager::mouseCallback(GLFWwindow* window, int button, int action, int mods)
{
	if (action == GLFW_PRESS || action == GLFW_RELEASE) {
		bool status = false;
		if (action == GLFW_PRESS)
			status = true;
		if (action == GLFW_RELEASE)
			status = false;
		if (button == GLFW_MOUSE_BUTTON_LEFT)
			_mouseKeys[0] = status;
		if (button == GLFW_MOUSE_BUTTON_RIGHT)
			_mouseKeys[1] = status;
		if (button == GLFW_MOUSE_BUTTON_MIDDLE)
			_mouseKeys[2] = status;
	}
}

/*static */void CallbackManager::addFuncToWRE(void (*function)(unsigned int, unsigned int))
{
	_funcsToCallWRE.push_back(function);
}

void CallbackManager::setProjectionMatrixCreationFunc(void (*function)(MasterRenderer *))
{
	_projectionMatrixCreationFunc = function;
}

void CallbackManager::setMasterRendererPtr(MasterRenderer *masterRendererPtr)
{
	_masterRendererPtr = masterRendererPtr;
}

std::vector<bool> CallbackManager::getMouseInput()
{
	return _mouseKeys;
}

std::vector<int> CallbackManager::getMousePosition()
{
	double mousePosX, mousePosY;
	glfwGetCursorPos(_window, &mousePosX, &mousePosY);
	_mousePosition[0] = static_cast<int>(mousePosX);
	_mousePosition[1] = static_cast<int>(mousePosY);
	return _mousePosition;
}

std::vector<unsigned int> CallbackManager::getWindowSize()
{
	int width, height;
	glfwGetWindowSize(_window, &width, &height);
	return {static_cast<unsigned int>(width), static_cast<unsigned int>(height)};
}
