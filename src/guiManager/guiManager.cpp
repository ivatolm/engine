#include "guiManager/guiManager.h"

void GuiManager::setWindow(GLFWwindow *window) {
	_window = window;
	int width, height;
	glfwGetWindowSize(_window, &width, &height);
	_windowSize = {width, height};
}

void GuiManager::setEntities(std::vector<InterfaceEntity *> interfaceEntities) {
	_interfaceEntities = interfaceEntities;
}

void GuiManager::updateWindowSize() {
	int width, height;
	glfwGetWindowSize(_window, &width, &height);
	_windowSize = {width, height};
}

void GuiManager::update(std::vector<int> mousePosition, std::vector<bool> mouseKeys) {
	updateWindowSize();

	glm::vec2 convertedMousePosition = {
		(mousePosition[0] / (_windowSize.x / 2)) - 1.0f,
		-((mousePosition[1] / (_windowSize.y / 2)) - 1.0f)
	};

	for (InterfaceEntity *interfaceEntity : _interfaceEntities) {
		if (!interfaceEntity->getIsClickable())
			continue;

		glm::vec2 leftTopCorner = interfaceEntity->getLeftTopCorner();
		glm::vec2 bottomRightCorner = interfaceEntity->getRightBottomCorner();

		interfaceEntity->setClickedStatus(false);
		interfaceEntity->setHoveredStatus(false);
		if ((convertedMousePosition.x > leftTopCorner.x && convertedMousePosition.y < leftTopCorner.y)
				&&
			(convertedMousePosition.x < bottomRightCorner.x && convertedMousePosition.y > bottomRightCorner.y))
		{
			interfaceEntity->setHoveredStatus(true);
			if (mouseKeys[0] == true)
				interfaceEntity->setClickedStatus(true);
		}
	}
}
