#include "shaders/shaderProgram.h"
#include <fstream>
#include <stdio.h>

ShaderProgram::ShaderProgram(const char *vertexShaderFile, const char *fragmentShaderFile)
{
	_vertexShaderId = loadShader(vertexShaderFile, GL_VERTEX_SHADER);
	_fragmentShaderId = loadShader(fragmentShaderFile, GL_FRAGMENT_SHADER);
}

ShaderProgram::~ShaderProgram() {
	stop();
	glDetachShader(_programId, _vertexShaderId);
	glDetachShader(_programId, _fragmentShaderId);
	glDeleteShader(_vertexShaderId);
	glDeleteShader(_fragmentShaderId);
	glDeleteProgram(_programId);
}

void ShaderProgram::start() {
	glUseProgram(_programId);
}

void ShaderProgram::stop() {
	glUseProgram(0);
}

void ShaderProgram::createProgram()
{
	_programId = glCreateProgram();
	glAttachShader(_programId, _vertexShaderId);
	glAttachShader(_programId, _fragmentShaderId);
	bindAttributes();
	glLinkProgram(_programId);
	glValidateProgram(_programId);
	getAllUniformLocations();
}

void ShaderProgram::loadFloat(int location, float value)
{
	glUniform1f(location, value);
}

void ShaderProgram::loadVector(int location, glm::vec3 vector)
{
	glUniform3f(location, vector.x, vector.y, vector.z);
}

void ShaderProgram::loadBool(int location, bool value)
{
	glUniform1f(location, value);
}

void ShaderProgram::loadMatrix(int location, glm::mat4 matrix)
{
	glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
}

void ShaderProgram::bindAttribute(int attributeId, const char *variableName)
{
	glBindAttribLocation(_programId, attributeId, variableName);
}

int ShaderProgram::getUniformLocation(const char *variableName)
{
	return glGetUniformLocation(_programId, variableName);
}

unsigned int ShaderProgram::loadShader(const char *pathToFile, GLenum type)
{
	std::string content;
		std::ifstream fileStream(pathToFile, std::ios::in);

		if(!fileStream.is_open()) {
			perror("Error then reading from shader file.\n");
		}

		std::string line = "";
		while(!fileStream.eof()) {
			std::getline(fileStream, line);
			content.append(line + "\n");
		}
		fileStream.close();

		GLuint shaderId = glCreateShader(type);
		const GLchar *source = content.c_str();
		glShaderSource(shaderId, 1, &source, nullptr);
		glCompileShader(shaderId);
		GLint success = 0;
		glGetShaderiv(shaderId, GL_COMPILE_STATUS, &success);
		if (success)
			printf("Shader successfully compiled.\n");
		else
			printf("Shader compilation failed.\n");
		return shaderId;
}
