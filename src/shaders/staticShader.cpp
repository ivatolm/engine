#include "shaders/staticShader.h"
#include "entities/lightEntity.h"

StaticShader::StaticShader(const char *vertexShaderFile, const char *fragmentShaderFile)
	: ShaderProgram(vertexShaderFile, fragmentShaderFile)
{
	this->ShaderProgram::createProgram();
}

void StaticShader::bindAttributes()
{
	this->ShaderProgram::bindAttribute(0, "position");
	this->ShaderProgram::bindAttribute(1, "textureCoords");
	this->ShaderProgram::bindAttribute(2, "normal");
}

void StaticShader::getAllUniformLocations()
{
	_locationTransformationMatrix = this->ShaderProgram::getUniformLocation("transformationMatrix");
	_locationProjectionMatrix = this->ShaderProgram::getUniformLocation("projectionMatrix");
	_locationViewMatrix = this->ShaderProgram::getUniformLocation("viewMatrix");
	_locationLightPosition = this->ShaderProgram::getUniformLocation("lightPosition");
	_locationLightColor = this->ShaderProgram::getUniformLocation("lightColor");
	_locationLightAttenuation = this->ShaderProgram::getUniformLocation("lightAttenuation");
}

void StaticShader::loadTransformationMatrix(glm::mat4 matrix)
{
	this->ShaderProgram::loadMatrix(_locationTransformationMatrix, matrix);
}

void StaticShader::loadProjectionMatrix(glm::mat4 matrix)
{
	this->ShaderProgram::loadMatrix(_locationProjectionMatrix, matrix);
}

void StaticShader::loadViewMatrix(glm::mat4 matrix)
{
	this->ShaderProgram::loadMatrix(_locationViewMatrix, matrix);
}

void StaticShader::loadLight(LightEntity lightEntity)
{
	this->ShaderProgram::loadVector(_locationLightPosition, lightEntity.getPosition());
	this->ShaderProgram::loadVector(_locationLightColor, lightEntity.getColor());
	this->ShaderProgram::loadVector(_locationLightAttenuation, lightEntity.getAttenuation());
}
