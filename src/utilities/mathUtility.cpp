#include "utilities/mathUtility.h"
#include "entities/cameraEntity.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

/*static */glm::mat4 MathUtility::createTransformationMatrix(glm::vec3 translation, glm::vec3 rotation, float scale)
{
	glm::mat4 transformationMatrix;
	glm::mat4 translationMatrix = glm::translate(translation);
	glm::mat4 rotationMatrixX = glm::rotate(glm::radians(rotation.x), glm::vec3(1, 0, 0));
	glm::mat4 rotationMatrixY = glm::rotate(rotationMatrixX, glm::radians(rotation.y), glm::vec3(0, 1, 0));
	glm::mat4 rotationMatrixZ = glm::rotate(rotationMatrixY, glm::radians(rotation.z), glm::vec3(0, 0, 1));
	glm::mat4 rotationMatrix = rotationMatrixZ;
	glm::mat4 scalingMatrix = glm::scale(glm::vec3(scale, scale, scale));
	transformationMatrix =  translationMatrix*rotationMatrix*scalingMatrix;
	return transformationMatrix;
}

/*static */glm::mat4 MathUtility::createProjectionMatrix(CameraEntity camera)
{
	glm::mat4 projectionMatrix = glm::perspective(camera.getFOV(), camera.getAspectRatio(), camera.getNearPlane(), camera.getFarPlane());
	return projectionMatrix;
}

/*static */glm::mat4 MathUtility::createViewMatrix(CameraEntity camera)
{
	glm::mat4 viewMatrix;
	glm::mat4 rotationMatrixX = glm::rotate(glm::radians(camera.getRotation().x), glm::vec3(1, 0, 0));
	glm::mat4 rotationMatrixY = glm::rotate(rotationMatrixX, glm::radians(camera.getRotation().y), glm::vec3(0, 1, 0));
	glm::vec3 cameraPos = camera.getPosition();
	glm::vec3 negativeCameraPos = glm::vec3(-cameraPos.x, -cameraPos.y, -cameraPos.z);
	glm::mat4 translationMatrix = glm::translate(negativeCameraPos);
	viewMatrix = rotationMatrixY * translationMatrix;
	return viewMatrix;
}

/*static */float MathUtility::barryCentric(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec2 pos) {
	float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
	float l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
	float l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
	float l3 = 1.0f - l1 - l2;
	return l1 * p1.y + l2 * p2.y + l3 * p3.y;
}
