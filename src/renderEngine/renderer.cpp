#include "renderEngine/renderer.h"
#include "entities/basicEntity.h"
#include "entities/interfaceEntity.h"
#include "entities/terrainEntity.h"
#include "shaders/staticShader.h"
#include "utilities/mathUtility.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>

void Renderer::renderBasicEntity(BasicEntity *basicEntity, StaticShader *basicEntityShader) {
	TexturedModel texturedModel = basicEntity->getModel();
	RawModel model = texturedModel.getModel();
	glBindVertexArray(model.getVAOId());
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glm::mat4 transformationMatrix = MathUtility::createTransformationMatrix(basicEntity->getPosition(), basicEntity->getRotation(), basicEntity->getScale());
	basicEntityShader->loadTransformationMatrix(transformationMatrix);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texturedModel.getTexture().getId());
	glDrawElements(GL_TRIANGLES, static_cast<int>(model.getVertexCount()), GL_UNSIGNED_INT, nullptr);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glBindVertexArray(0);
}

void Renderer::renderInterfaceEntity(InterfaceEntity *interfaceEntity, StaticShader *interfaceEntityShader) {
	RawModel model = interfaceEntity->getModel();
	glBindVertexArray(model.getVAOId());
	glEnableVertexAttribArray(0);
	glm::mat4 transformationMatrix = MathUtility::createTransformationMatrix(interfaceEntity->getPosition(), interfaceEntity->getRotation(), interfaceEntity->getScale());
	interfaceEntityShader->loadTransformationMatrix(transformationMatrix);
	glDrawElements(GL_TRIANGLES, static_cast<int>(model.getVertexCount()), GL_UNSIGNED_INT, nullptr);
	glDisableVertexAttribArray(0);
	glBindVertexArray(0);
}

void Renderer::renderTerrainEntity(TerrainEntity *terrainEntity, StaticShader *terrainEntityShader)
{
	RawModel model = terrainEntity->getModel();
	glBindVertexArray(model.getVAOId());
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(2);
	glm::mat4 transformationMatrix = MathUtility::createTransformationMatrix(terrainEntity->getPosition(), terrainEntity->getRotation(), terrainEntity->getScale());
	terrainEntityShader->loadTransformationMatrix(transformationMatrix);
	glDrawElements(GL_TRIANGLES, static_cast<int>(model.getVertexCount()), GL_UNSIGNED_INT, nullptr);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(2);
	glBindVertexArray(0);
}
