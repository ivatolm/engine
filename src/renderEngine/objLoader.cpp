#include "renderEngine/objLoader.h"
#include "models/rawModel.h"
#include <fstream>
#include <iostream>

void ObjLoader::setLoader(Loader *loader) {
	_loader = loader;
}

RawModel ObjLoader::loadModel(const char *pathToFile) {
	if (_loader == nullptr) {
		printf("ObjLoader: Loader ptr in location nullptr\n");
	}

	std::vector<std::string> content;
	std::ifstream fileStream(pathToFile, std::ios::in);

	if(!fileStream.is_open())
		std::cerr << "Error then reading from obj file." << std::endl;

	std::string line = "";
	while(!fileStream.eof()) {
		std::getline(fileStream, line);
		content.push_back(line);
	}
	fileStream.close();

	std::vector<glm::vec3> inVertices;
	std::vector<glm::vec2> inTextureCoords;
	std::vector<glm::vec3> inNormals;

	unsigned int lastLine = 0;
	for (unsigned int lineNum = 0; lineNum < content.size(); lineNum++) {
		std::string line = content[lineNum];
		if (line[0] == 'v' && line[1] == ' ') {
			std::vector<float> floats = findFloats(line, 2);
			if (floats.size() == 3)
				inVertices.push_back({floats[0], floats[1], floats[2]});
		}
		if (line[0] == 'v' && line[1] == 't' && line[2] == ' ') {
			std::vector<float> floats = findFloats(line, 3);
			if (floats.size() == 2)
				inTextureCoords.push_back({floats[0], floats[1]});
		}
		if (line[0] == 'v' && line[1] == 'n' && line[2] == ' ') {
			std::vector<float> floats = findFloats(line, 3);
			if (floats.size() == 3)
				inNormals.push_back({floats[0], floats[1], floats[2]});
		}
		if (line[0] == 'f') {
			lastLine = lineNum;
			break;
		}
	}

	std::vector<float> outVertices;
	std::vector<float> outTextureCoords(inVertices.size() * 2);
	std::vector<unsigned int> outIndices(inVertices.size() * 3);
	std::vector<float> outNormals(inVertices.size() * 3);

	for (unsigned int lineNum = lastLine; lineNum < content.size(); lineNum++) {
		std::string line = content[lineNum];
		if (line[0] == 'f' && line[1] == ' ') {
			std::vector<int> ints = findInts(line, 2);
			if (ints.size() != 9)
				continue;
			for (unsigned int i = 0 ; i < ints.size(); i++) {
				unsigned int num = static_cast<unsigned int>(ints[i]-1);
				if (i % 3 == 0) {
					outIndices.push_back(num);
				}
				if (i % 3 == 1) {
					unsigned int index = static_cast<unsigned int>(ints[i-1] - 1);
					glm::vec2 currentTexture = inTextureCoords[num];
					outTextureCoords[index * 2 + 0] = currentTexture.x;
					outTextureCoords[index * 2 + 1] = 1 - currentTexture.y;
				}
				if (i % 3 == 2) {
					unsigned int index = static_cast<unsigned int>(ints[i-2] - 1);
					glm::vec3 currentNormal = inNormals[num];
					outNormals[index * 3 + 0] = currentNormal.x;
					outNormals[index * 3 + 1] = currentNormal.y;
					outNormals[index * 3 + 2] = currentNormal.z;
				}
			}
		}
	}

	for (glm::vec3 vertex : inVertices) {
		outVertices.push_back(vertex.x);
		outVertices.push_back(vertex.y);
		outVertices.push_back(vertex.z);
	}

	return _loader->loadEntity(outVertices, outIndices, outTextureCoords, outNormals);
}

std::vector<float> ObjLoader::findFloats(std::string line, unsigned int startFrom)
{
	std::vector<float> nums;
	unsigned long lineLength = line.size();
	unsigned long i = startFrom;
	while (i < lineLength) {
		std::string numberString;
		while (i < lineLength && line[i] != ' ' && line[i] != '/') {
			numberString += line[i];
			i++;
		}
		i++;
		nums.push_back(std::stof(numberString));
	}
	return nums;
}

std::vector<int> ObjLoader::findInts(std::string line, unsigned int startFrom)
{
	std::vector<int> nums;
	unsigned long lineLength = line.size();
	unsigned long i = startFrom;
	while (i < lineLength) {
		std::string numberString;
		while (i < lineLength && line[i] != ' ' && line[i] != '/') {
			numberString += line[i];
			i++;
		}
		i++;
		nums.push_back(std::stoi(numberString));
	}
	return nums;
}
