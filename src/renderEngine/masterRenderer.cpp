#include "renderEngine/masterRenderer.h"
#include "entities/cameraEntity.h"
#include "entities/terrainEntity.h"
#include "entities/lightEntity.h"
#include "shaders/staticShader.h"
#include "utilities/mathUtility.h"
#include <assert.h>

MasterRenderer::MasterRenderer() {
	glewExperimental = GL_TRUE;
	glewInit();

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	_renderer = *new Renderer;
}

void MasterRenderer::render(std::vector<BasicEntity *> basicEntities, std::vector<InterfaceEntity *> interfaceEntities, std::vector<TerrainEntity *> terrainEntities, std::vector<LightEntity *> lightEntities) {
	prepare();

	glm::mat4 viewMatrix = MathUtility::createViewMatrix(*_camera);

	_basicEntityShader->start();
	_basicEntityShader->loadLight(*lightEntities[0]);
	_basicEntityShader->loadViewMatrix(viewMatrix);
	for (BasicEntity *basicEntity : basicEntities)
		_renderer.renderBasicEntity(basicEntity, _basicEntityShader);
	_basicEntityShader->stop();

	_interfaceEntityShader->start();
	_interfaceEntityShader->loadViewMatrix(viewMatrix);
	for (InterfaceEntity *interfaceEntity : interfaceEntities)
		_renderer.renderInterfaceEntity(interfaceEntity, _interfaceEntityShader);
	_interfaceEntityShader->stop();

	_terrainEntityShader->start();
	_terrainEntityShader->loadLight(*lightEntities[0]);
	_terrainEntityShader->loadViewMatrix(viewMatrix);
	for (TerrainEntity *terrainEntity : terrainEntities)
		_renderer.renderTerrainEntity(terrainEntity, _terrainEntityShader);
	_terrainEntityShader->stop();

	swapBuffers();
}

void MasterRenderer::setRenderingWindow(GLFWwindow *window) {
	_window = window;
}

void MasterRenderer::setCamera(CameraEntity *camera)
{
	_camera = camera;
}

void MasterRenderer::setShaders(StaticShader *basicEntityShader, StaticShader *interfaceEntityShader, StaticShader *terrainEntityShader)
{
	_basicEntityShader = basicEntityShader;
	_interfaceEntityShader = interfaceEntityShader;
	_terrainEntityShader = terrainEntityShader;
}

void MasterRenderer::setProjectionMatrix(MasterRenderer *instance)
{
	assert(instance);
	if (instance)
		instance->_setProjectionMatrix();
}

void MasterRenderer::_setProjectionMatrix()
{
	glm::mat4 projectionMatrix = MathUtility::createProjectionMatrix(*_camera);
	_basicEntityShader->start();
	_basicEntityShader->loadProjectionMatrix(projectionMatrix);
	_basicEntityShader->stop();
	_interfaceEntityShader->start();
	_interfaceEntityShader->loadProjectionMatrix(projectionMatrix);
	_interfaceEntityShader->stop();
	_terrainEntityShader->start();
	_terrainEntityShader->loadProjectionMatrix(projectionMatrix);
	_terrainEntityShader->stop();
}

void MasterRenderer::prepare() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0, 0, 0, 1);
}

void MasterRenderer::swapBuffers() {
	glfwSwapBuffers(_window);
}
