#include "renderEngine/loader.h"
#include "models/rawModel.h"
#include <SOIL/SOIL.h>

RawModel Loader::loadEntity(std::vector<float> verticesCoords, std::vector<unsigned int> indices, std::vector<float> texturesCoords, std::vector<float> normals)
{
	unsigned int VAOId = createVAO();
	bindIndicesBuffer(indices);
	storeDataInAttributeList(0, 3, verticesCoords);
	storeDataInAttributeList(1, 2, texturesCoords);
	storeDataInAttributeList(2, 3, normals);
	unBindVAO();
	return RawModel(VAOId, static_cast<unsigned int>(indices.size()));
}

RawModel Loader::loadTerrainEntity(std::vector<float> verticesCoords, std::vector<unsigned int> indices, std::vector<float> normals)
{
	unsigned int VAOId = createVAO();
	bindIndicesBuffer(indices);
	storeDataInAttributeList(0, 3, verticesCoords);
	storeDataInAttributeList(2, 3, normals);
	unBindVAO();
	return RawModel(VAOId, static_cast<unsigned int>(indices.size()));
}

unsigned int Loader::loadTexture(const char *pathToFile)
{
	int textureWidth, textureHeight;
	unsigned char* texture = SOIL_load_image(pathToFile, &textureWidth, &textureHeight, nullptr, SOIL_LOAD_RGBA);
	unsigned int textureId;
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureWidth, textureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture);
	glGenerateMipmap(GL_TEXTURE_2D);
	SOIL_free_image_data(texture);
	glBindTexture(GL_TEXTURE_2D, 0);
	_textures.push_back(textureId);
	return textureId;
}

void Loader::cleanUp()
{
	for(unsigned int VAO : _VAOs)
		glDeleteVertexArrays(1, &VAO);
	for(unsigned int VBO : _VBOs)
		glDeleteBuffers(1, &VBO);
	for (unsigned int texture : _textures)
		glDeleteTextures(1, &texture);
}

unsigned int Loader::createVAO()
{
	unsigned int VAOId = static_cast<unsigned int>(_VAOs.size());
	_VAOs.push_back(VAOId);
	glGenVertexArrays(1, &VAOId);
	glBindVertexArray(VAOId);
	return VAOId;
}

void Loader::storeDataInAttributeList(int attributeNumber, int coordNum, std::vector<float> data)
{
	unsigned int VBOId = static_cast<unsigned int>(_VBOs.size());
	_VBOs.push_back(VBOId);
	glGenBuffers(1, &VBOId);
	glBindBuffer(GL_ARRAY_BUFFER, VBOId);
	glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(float), data.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(attributeNumber, coordNum, GL_FLOAT, false, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Loader::bindIndicesBuffer(std::vector<unsigned int> indices)
{
	GLuint VBOId = static_cast<unsigned int>(_VBOs.size());
	_VBOs.push_back(VBOId);
	glGenBuffers(1, &VBOId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOId);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), indices.data(), GL_STATIC_DRAW);
}

void Loader::unBindVAO()
{
	glBindVertexArray(0);
}
