#include "physics/physicsManager.h"

void PhysicsManager::update(float deltaTime)
{
	for (BasicEntity *entity : _basicEntities) {
		glm::vec3 entityPosition = entity->getPosition();
		glm::vec3 entityVelocity = entity->getVelocity();
		glm::vec3 entityAcceleration = entity->getAcceleration();

		entityAcceleration.y = -GRAVITY;
		entityVelocity.y += entityAcceleration.y * deltaTime;
		if (entityVelocity.y > 1.0f)
			entityVelocity.y = 1.0f;
		if (entityVelocity.y < -1.0f)
			entityVelocity.y = -1.0f;
		entityPosition.y += entityVelocity.y;

		for (TerrainEntity *terrainEntity : _terrainEntities) {
			std::pair<bool, float> result = terrainEntity->getVertexHeight(entityPosition.x, entityPosition.z);
			if (result.first == false)
				continue;
			float vertexHeight = result.second;
			if (entityPosition.y < vertexHeight)
				entityPosition = {entityPosition.x, vertexHeight, entityPosition.z};
		}
		entity->setPosition(entityPosition);
	}
}

void PhysicsManager::setBasicEntities(std::vector<BasicEntity *> basicEntities)
{
	_basicEntities = basicEntities;
}

void PhysicsManager::addBasicEntity(BasicEntity *basicEntity)
{
	_basicEntities.push_back(basicEntity);
}

void PhysicsManager::setTerrainEntities(std::vector<TerrainEntity *> terrainEntities)
{
	_terrainEntities = terrainEntities;
}

void PhysicsManager::addTerrainEntity(TerrainEntity *terrainEntity)
{
	_terrainEntities.push_back(terrainEntity);
}
