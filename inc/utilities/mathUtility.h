#pragma once

#include "entities/cameraEntity.h"
#include <glm/glm.hpp>

class CameraEntity;

class MathUtility
{
public:
	static glm::mat4 createTransformationMatrix(glm::vec3 translation, glm::vec3 rotation, float scale);
	static glm::mat4 createProjectionMatrix(CameraEntity camera);
	static glm::mat4 createViewMatrix(CameraEntity camera);
	static float barryCentric(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec2 pos);
};
