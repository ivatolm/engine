#pragma once

#include "entities/interfaceEntity.h"
#include <GLFW/glfw3.h>
#include <vector>

class GuiManager
{
public:
	void setWindow(GLFWwindow *window);
	void setEntities(std::vector<InterfaceEntity *> interfacentities);
	void updateWindowSize();
	void update(std::vector<int> mousePosition, std::vector<bool> mouseKeys);
private:
	GLFWwindow *_window;
	glm::vec2 _windowSize;
	std::vector<InterfaceEntity *> _interfaceEntities;
};
