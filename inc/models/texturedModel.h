#pragma once

#include "models/rawModel.h"
#include "textures/texture.h"

class TexturedModel : public RawModel
{
public:
	TexturedModel(RawModel model, Texture texture);
	RawModel getModel();
	Texture getTexture();
private:
	RawModel _model;
	Texture _texture;
};
