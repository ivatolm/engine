#pragma once

class RawModel
{
private:
	unsigned int _VAOId;
	unsigned int _vertexCount;
public:
	RawModel(unsigned int VAOId, unsigned int vertexCount);
	unsigned int getVAOId() const;
	unsigned int getVertexCount() const;
};
