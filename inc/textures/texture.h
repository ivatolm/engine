#pragma once

class Texture
{
public:
	Texture(unsigned int textureId);
	unsigned int getId();
private:
	unsigned int _textureId;
};
