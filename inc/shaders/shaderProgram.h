#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/type_ptr.hpp>

class ShaderProgram
{
public:
	ShaderProgram(const char *vertexShaderFile, const char *fragmentShaderFile);
	virtual ~ShaderProgram();

	void start();
	void stop();
protected:
	void createProgram();
	void loadFloat(int location, float value);
	void loadVector(int location, glm::vec3 vector);
	void loadBool(int location, bool value);
	void loadMatrix(int location, glm::mat4 matrix);
	void bindAttribute(int attributeId, const char *variableName);
	int getUniformLocation(const char *variableName);
	virtual void bindAttributes() = 0;
	virtual void getAllUniformLocations() = 0;
private:
	unsigned int loadShader(const char *pathToFile, GLenum type);
	unsigned int _programId;
	unsigned int _vertexShaderId;
	unsigned int _fragmentShaderId;
};
