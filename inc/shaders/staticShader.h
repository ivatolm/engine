#pragma once

#include "shaders/shaderProgram.h"

class LightEntity;

class StaticShader : public ShaderProgram
{
public:
	StaticShader(const char *vertexShaderFile, const char *fragmentShaderFile);
	void bindAttributes() override;
	void getAllUniformLocations() override;
	void loadTransformationMatrix(glm::mat4 matrix);
	void loadProjectionMatrix(glm::mat4 matrix);
	void loadViewMatrix(glm::mat4 matrix);
	void loadLight(LightEntity lightEntity);
private:
	int _locationTransformationMatrix;
	int _locationProjectionMatrix;
	int _locationViewMatrix;
	int _locationLightPosition;
	int _locationLightColor;
	int _locationLightAttenuation;
};
