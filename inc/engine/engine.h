#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "displayManager/displayManager.h"
#include "displayManager/callbackManager.h"
#include "renderEngine/masterRenderer.h"
#include "renderEngine/loader.h"
#include "renderEngine/objLoader.h"
#include "guiManager/guiManager.h"
#include "shaders/staticShader.h"
#include "entities/cameraEntity.h"
#include "entities/basicEntity.h"
#include "entities/interfaceEntity.h"
#include "entities/terrainEntity.h"
#include "entities/lightEntity.h"
#include "physics/physicsManager.h"

class Engine
{
public:
	Engine();
	void update();
	bool isRunning();
	void setEntityShader(const char *vertexShaderFile, const char *fragmentShaderFile);
	void setInterfaceShader(const char *vertexShaderFile, const char *fragmentShaderFile);
	void setTerrainShader(const char *vertexShaderFile, const char *fragmentShaderFile);
	void setCameraTarget(RawEntity *entity);
	void addBasicEntity(BasicEntity *basicEntity);
	void addInterfaceEntity(InterfaceEntity *interfaceEntity);
	void addTerrainEntity(TerrainEntity *terrainEntity);
	void addLightEntity(LightEntity *lightEntity);
	RawModel loadModel(const char *pathToFile);
	unsigned int loadTexture(const char *pathToFile);
private:
	DisplayManager *_displayManager;
	CallbackManager *_callbackManager;
	MasterRenderer *_masterRenderer;
	Loader *_loader;
	ObjLoader *_objLoader;
	GuiManager *_guiManager;
	PhysicsManager *_physicsManager;
	StaticShader *_basicEntityShader;
	StaticShader *_interfaceEntityShader;
	StaticShader *_terrainEntityShader;
	CameraEntity *_camera;
	std::vector<unsigned int> _windows;
	std::vector<BasicEntity *> _basicEntities;
	std::vector<InterfaceEntity *> _interfaceEntities;
	std::vector<TerrainEntity *> _terrainEntities;
	std::vector<LightEntity *> _lightEntities;
	bool _running;
};
