#pragma once

#include "entities/basicEntity.h"
#include "entities/terrainEntity.h"
#include <vector>

class PhysicsManager
{
public:
	void update(float deltaTime);
	void setBasicEntities(std::vector<BasicEntity *> basicEntities);
	void addBasicEntity(BasicEntity *basicEntity);
	void setTerrainEntities(std::vector<TerrainEntity *> terrainEntities);
	void addTerrainEntity(TerrainEntity *terrainEntity);
private:
	const float GRAVITY = 9.8f;

	std::vector<BasicEntity *> _basicEntities;
	std::vector<TerrainEntity *> _terrainEntities;
};
