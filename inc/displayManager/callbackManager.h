#pragma once

#include "entities/cameraEntity.h"
#include "shaders/staticShader.h"
#include <vector>

class GLFWwindow;
class MasterRenderer;

class CallbackManager
{
public:
	void setUpCallbacks(GLFWwindow *window);
	static void window_size_callback(GLFWwindow *window, int width, int height);
	static void mouseCallback(GLFWwindow *window, int button, int action, int mods);
	static void addFuncToWRE(void (*function)(unsigned int, unsigned int));
	static void setProjectionMatrixCreationFunc(void (*function)(MasterRenderer *));
	void setMasterRendererPtr(MasterRenderer *masterRendererPtr);
	std::vector<bool> getMouseInput();
	std::vector<int> getMousePosition();
	std::vector<unsigned int> getWindowSize();
private:
	GLFWwindow *_window;
	static MasterRenderer *_masterRendererPtr;
	static std::vector<void (*)(unsigned int, unsigned int)> _funcsToCallWRE;
	static void (*_projectionMatrixCreationFunc)(MasterRenderer *);
	static std::vector<bool> _mouseKeys;
	static std::vector<int> _mousePosition;
};
