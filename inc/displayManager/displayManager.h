#pragma once

#include <vector>
#include <GLFW/glfw3.h>

class DisplayManager
{
public:
	DisplayManager();
	~DisplayManager();
	unsigned int createWindow(int width, int height, const char *title, bool fullscreen);
	void deleteWindow(unsigned int windowId);
	void makeContextCurrent(unsigned int windowId);
	GLFWwindow * getWindowPtr(unsigned int windowId);
	float getDeltaTime();
	bool isWindowClosed(unsigned int windowId);
	void setVSYNC(bool state);
	void setMouseHidden(bool state);
private:
	std::vector<std::pair<unsigned int, GLFWwindow *>> _windowsPtrs;
	unsigned int _windowsCounter;
	GLFWwindow *_currentWindow;
};
