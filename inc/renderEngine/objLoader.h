#pragma once

#include "renderEngine/loader.h"
#include <string>
#include <glm/glm.hpp>

class ObjLoader
{
public:
	void setLoader(Loader *loader);
	RawModel loadModel(const char *pathToFile);
private:
	std::vector<float> findFloats(std::string line, unsigned int startFrom);
	std::vector<int> findInts(std::string line, unsigned int startFrom);
	Loader *_loader = nullptr;
};
