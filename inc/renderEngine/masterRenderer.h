#pragma once

#include "renderEngine/renderer.h"
#include "entities/cameraEntity.h"
#include "shaders/staticShader.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <vector>

class CameraEntity;
class TerrainEntity;
class StaticShader;

class MasterRenderer
{
public:
	MasterRenderer();
	void render(std::vector<BasicEntity *> basicEntities, std::vector<InterfaceEntity *> interfaceEntities, std::vector<TerrainEntity *> terrainEntities, std::vector<LightEntity *> lightEntities);
	void setRenderingWindow(GLFWwindow *window);
	void setCamera(CameraEntity *camera);
	void setShaders(StaticShader *basicEntityShader, StaticShader *interfaceShader, StaticShader *terrainEntityShader);
	static void setProjectionMatrix(MasterRenderer *instance);
private:
	void _setProjectionMatrix();
	void prepare();
	void swapBuffers();
	Renderer _renderer;
	GLFWwindow *_window;

	CameraEntity *_camera;
	StaticShader *_basicEntityShader;
	StaticShader *_interfaceEntityShader;
	StaticShader *_terrainEntityShader;
};
