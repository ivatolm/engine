#pragma once

#include <vector>
#include <GL/glew.h>

class RawModel;

class Loader
{
public:
	RawModel loadEntity(std::vector<float> verticesCoords, std::vector<unsigned int> indices, std::vector<float> texturesCoords, std::vector<float> normals);
	RawModel loadTerrainEntity(std::vector<float> verticesCoords, std::vector<unsigned int> indices, std::vector<float> normals);
	unsigned int loadTexture(const char *pathToFile);
	void cleanUp();
private:
	std::vector<unsigned int> _VAOs;
	std::vector<unsigned int> _VBOs;
	std::vector<unsigned int> _textures;
	void storeDataInAttributeList(int attributeNumber, int coordNum, std::vector<float> data);
	void bindIndicesBuffer(std::vector<unsigned int> indices);
	unsigned int createVAO();
	void unBindVAO();
};
