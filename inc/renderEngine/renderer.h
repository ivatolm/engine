#pragma once

class BasicEntity;
class InterfaceEntity;
class TerrainEntity;
class StaticShader;

class Renderer
{
public:
	void renderBasicEntity(BasicEntity *basicEntity, StaticShader *basicEntityShader);
	void renderInterfaceEntity(InterfaceEntity *interfaceEntity, StaticShader *interfaceEntityShader);
	void renderTerrainEntity(TerrainEntity *terrainEntity, StaticShader *terrainEntityShader);
};
