#pragma once

#include "entities/rawEntity.h"

class LightEntity : public RawEntity
{
public:
	LightEntity(glm::vec3 position, glm::vec3 rotation, glm::vec3 color, glm::vec3 attenuation={1, 0, 0});
	glm::vec3 getColor();
	glm::vec3 getAttenuation();
	void setColor(glm::vec3 color);
	void setAttenuation(glm::vec3 attenuation);
private:
	glm::vec3 _color;
	glm::vec3 _attenuation;
};
