#pragma once

#include "entities/rawEntity.h"
#include <vector>

class Loader;

class TerrainEntity : public RawEntity
{
public:
	TerrainEntity(glm::vec3 position, unsigned int verticesOnSideNum, float size, float amplitude);
	void loadModel(Loader *loader);
	void generateHeights();
	std::vector<float> generateTerrainMesh();
	std::vector<unsigned int> generateIndices();
	std::pair<bool, float> getVertexHeight(float x, float z);
	std::vector<float> generateNormals();
private:
	unsigned int _vertexOnSideNum;
	float _size;
	float _amplitude;
	std::vector<std::vector<float>> _heights;
};
