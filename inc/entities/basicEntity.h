#pragma once

#include "entities/rawEntity.h"
#include "models/texturedModel.h"

class BasicEntity : public RawEntity
{
public:
	BasicEntity(TexturedModel model, glm::vec3 position, glm::vec3 rotation, float scale);
	TexturedModel getModel();
	glm::vec3 getVelocity();
	glm::vec3 getAcceleration();
	void setVelocity(glm::vec3 velocity);
	void setAcceleration(glm::vec3 acceleration);
private:
	TexturedModel _model;
	glm::vec3 _velocity;
	glm::vec3 _acceleration;
};
