#pragma once

#include "entities/rawEntity.h"
#include <vector>

class GLFWwindow;

class CameraEntity : public RawEntity
{
public:
	CameraEntity();
	void update(std::vector<int> mousePosition);
	static void setWindowSize(unsigned int width, unsigned int height);
	void setFollowingTarget(RawEntity *entity);
	float getFOV();
	float getAspectRatio();
	float getNearPlane();
	float getFarPlane();
private:
	float _fov;
	static float _aspectRatio;
	float _nearPlane;
	float _farPlane;

	RawEntity *_target;
	std::vector<int> _lastMousePosition;
	float _sensitivity;
	glm::vec3 _follObjOffset;
};
