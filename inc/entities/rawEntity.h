#pragma once

#include "models/rawModel.h"
#include <glm/glm.hpp>

class RawEntity
{
public:
	void increasePosition(glm::vec3 delta);
	void increaseRotataion(glm::vec3 delta);
	RawModel getModel();
	glm::vec3 getPosition();
	glm::vec3 getRotation();
	float getScale();
	void setPosition(glm::vec3 position);
	void setRotation(glm::vec3 rotation);
	void setScale(float scale);
protected:
	RawEntity(RawModel model, glm::vec3 position, glm::vec3 rotation, float scale);
	RawModel _model;
	glm::vec3 _position;
	glm::vec3 _rotation;
	float _scale;
};
