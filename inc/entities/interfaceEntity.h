#pragma once

#include "entities/rawEntity.h"

class InterfaceEntity : public RawEntity
{
public:
	InterfaceEntity(RawModel model, glm::vec2 leftTopCorner, glm::vec2 rightBottomCorner, bool isClickable);
	glm::vec2 getLeftTopCorner();
	glm::vec2 getRightBottomCorner();
	bool getIsClickable();
	void setClickedStatus(bool status);
	void setHoveredStatus(bool status);
	bool isClicked();
	bool isHovered();
private:
	glm::vec2 _leftTopCorner;
	glm::vec2 _rightBottomCorner;
	bool _isClickable;
	bool _clicked;
	bool _hovered;
};
